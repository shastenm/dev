def merge_sort(array):
    if len(array) > 1:

        midpoint = len(array)//2
        left_array = array[:midpoint]
        right_array = array[midpoint:]

        merge_sort(left_array)
        #print(left_array)
        merge_sort(right_array)
        #print(right_array)
        x = 0
        y = 0
        z = 0

        while x < len(left_array) and y < len(right_array):
            if left_array[x] < right_array[y]:
                array[z] = left_array[x]
                x += 1

            else:
                array[z] = right_array[y]
                y += 1 
            z += 1
        print(array) 
        while x < len(left_array):
            array[z] = left_array[x]
            x += 1
            z += 1
        #print(array)
        while y < len(right_array):
            array[z] = right_array[y]
            y += 1
            z += 1
        #print(array)

def print_sorted_array(array):
    for i in range(len(array)):
        print(array[i], end="")
    print()

if __name__=='__main__':
    numbers = [4, 10, 6, 14, 2, 1, 8, 5]
    
    merge_sort(numbers)

    print("Sorted array: ")
    print_sorted_array(numbers)
    