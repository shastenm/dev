import random

word_list = ['appearance', 'before', 'chevrolet', 'delta', 'fisherman', 'gear', 'homemaker']
random.shuffle(word_list)

def guess_letter():
    return input('Guess a letter: ').lower()
my_letter = guess_letter()

def get_dashes():
    return len(word_list[0]) * '*'
dashes = get_dashes()


if __name__ == "__main__":
    for letter in word_list[0]:
        if letter in word_list[0]:
            updated_string = letter.join(dashes)
            print(updated_string)

