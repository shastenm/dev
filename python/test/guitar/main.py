from webbrowser import get
from colorama import init, Fore

init()

NOTES = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'] * 3
IONIAN = [0, 2, 4, 5, 7, 9, 11]
DORIAN = [2, 4, 5, 7, 9, 11, 12]
PHRYGIAN = [4, 5, 7, 9, 11, 12, 14]
LYDIAN = [5, 7, 9, 11, 12, 14, 16]
MIXOLYDIAN = [7, 9, 11, 12, 14, 16, 17]
AEOLIAN = [9, 11, 12, 14, 16, 17, 19]
LOCRIAN = [11, 12, 14, 16, 17, 19, 21]
ROOT = input('Enter Root: ').upper()
START = int(input('Enter fret number: ')) #no_of_strings = int(input("Enter string number: ")) #colored(IONIAN.index(ROOT), "red", "green", "yellow", "blue", "magenta", "cyan", "White")
no_of_strings = int(input("Enter Number of Strings: "))
def get_string_notes():
    notes = input('Enter notes: ').upper().split()
    return notes
string_notes = get_string_notes()

notes = string_notes[0:no_of_strings]
tuning = map(NOTES.index, notes)

def get_major_scale():
    """This returns major scale notes and establishes a variable 'major_scale'"""
    return [NOTES[(y+NOTES.index(ROOT))%len(NOTES)] for y in IONIAN]
     

#get_major_scale()
major_scale = get_major_scale()

# def helper(i)
#   strings = ...
#   for n in strings:
#map(helper, tuning)

def print_notes():
    for i in tuning:
        i = i + START
        strings = NOTES[i:i+16]

        for n in strings:
            if n in major_scale:
                
                if len(n) == '1':
                    print(n,"             ", end="")
                else:
                    print(n,"            ", end="")
                    
            else:
                print("           ", end ="")
        print('')

print_notes()
print('_____' * 40)

for i in range(START, START+16):
    if i % 2 == 0:
        print(i,"           ", end="")
    elif i % 2 == 1 :
        print(i,"          ", end="")

print("") 


