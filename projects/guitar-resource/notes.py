#MUSIC NOTES LIST
NOTES = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'] * 3

#CIRCULAR FUNCTION FOR LIST
#if NOTES > NOTES[-1]:
#    NOTES = NOTES[0]

#SCALES
MAJOR_SCALE = [0, 2, 4, 5, 7, 9, 11]
NATURAL_MINOR_SCALE = [0, 2, 3, 5, 7, 8, 10]
HARMONIC_MINOR_SCALE = [0, 2, 3, 5, 7, 8, 11]
PENTATONIC_MINOR_SCALE = [0, 3, 5, 8, 10]

#CHORDS
MAJOR = [0, 4, 7]
SEVEN = [0, 2, 5, 7]
MAJOR_7 = [0, 4, 7, 11]
SIXTH = [0, 4, 7, 9]
MINOR = [0, 3, 7]
MINOR_7 = [0, 3, 7, 10]
MINOR_6 = [0, 3, 7, 9]
SUS_2 = [0, 2, 7]
SUS_4 = [0, 5, 7]
SEVEN_SUS_2 = [0, 2, 7, 10]
SEVEN_SUS_4 = [0, 5, 7, 10]
DIMINISHED = [0, 3, 6]
DIMINISHED_7 = [0, 3, 6, 9]
AUGMENTED = [0, 4, 8]
FIFTH = [0, 5]
M_MAJOR_7 = [0, 3, 4, 7]


