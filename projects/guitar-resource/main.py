from notes import *
from scale_finder import *
import PySimpleGUI as sg

sg.theme('Topanga')




layout = [    [sg.Text('Number Of Strings'), sg.InputText(), sg.Text('Key'), sg.Combo(NOTES), sg.Text('Capo'), sg.InputText(),
               sg.Text('Scale'), sg.Combo(values=None), 
               sg.Text('Enter String notes'), sg.InputText(), sg.Button('LOAD'),
               sg.Canvas(),
               sg.Button('Refresh'), sg.Button('Quit')]]

#Create window
window = sg.Window('Window Title', layout)

#Event Loop to process "events"
while True:
    event, values = window.read()
    if event in (sg.WIN_CLOSED, 'Quit'):
        break
window.close()

