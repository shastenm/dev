# word = 'banana'
# dashes = "_" * len(word)
#
# user_input = input('Enter in a letter: ')
# guessed = []
# out = ''
#
# for letter in word:
#    if letter in guessed:
#        out = out + letter
#    else:
#        out = out + '_'
#        print(out)
#
#
def find_max(nums):
    max_num = float("-inf")
    for num in nums:
        if num > max_num:
            max_num = num
    return max_num
