#!/usr/bin/env python3

from tkinter import *
import time 
from beepy import *
import sys

TIMELAPSE = 60

HOUR = int(input("How many hours: "))
MINUTES = int(input("How many minutes? "))
SECONDS = int(input("How many seconds? "))

TOTAL = HOUR * TIMELAPSE ** 2  + MINUTES * TIMELAPSE + SECONDS

while True:
    
    STOP = int(TOTAL)
    
    while STOP > 0:
        m, s = divmod(STOP, TIMELAPSE)
        h, m = divmod(m, TIMELAPSE)
        h = str(h)
        m = str(m)
        s = str(s)
        print(f"{h.zfill(2)}:{m.zfill(2)}:{s.zfill(2)}", end="\r")
        time.sleep(1)
        STOP -= 1
    print()
    beep(sound="ping")
    sys.exit()
